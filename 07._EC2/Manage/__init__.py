import boto3
import appconfig

#This code will create an EC2 instance
AMI_ID='ami-0470e33cd681b2476'
INSTANCE_TYPE ='t2.micro'

ec2Resource = boto3.resource('ec2',aws_access_key_id =appconfig.get('AWSConfig','aws.accessKey')
                             , aws_secret_access_key=appconfig.get('AWSConfig','aws.accessKeySecret')
                             , region_name=appconfig.get('AWSConfig', 'aws.region'))

#To load key pair from AWS
#key_pair = ec2Resource.KeyPair('ec2KeyPair')

#print(key_pair.name)



#Create EC2 instance
instances = ec2Resource.create_instances(
    ImageId=AMI_ID,
    MinCount=1,
    MaxCount=1,
    InstanceType=INSTANCE_TYPE,
    KeyName=appconfig.get('AWSConfig','aws.ec2KeyPair')
)

print(instances)
print("Instance created successfully.")
