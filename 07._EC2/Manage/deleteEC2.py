import boto3
import json
import appconfig

"""
ACCESS_KEY_ID = 'ASDASDDA'
ACCESS_KEY_SECRET = 'ASFWERSDFD'
AWS_REGION= 'ap-south-1'
"""
ec2client = boto3.client('ec2', aws_access_key_id =appconfig.get('AWSConfig','aws.accessKey')
                         , aws_secret_access_key=appconfig.get('AWSConfig','aws.accessKeySecret')
                         , region_name=appconfig.get('AWSConfig', 'aws.region'));

response = ec2client.describe_instances()

print("InstanceID\t\tPublicIP")
print("---------------------------------------")

# Get all running instances IDs and add it to the delInstanceArray
delInstanceArray=[]
for reservation in response["Reservations"]:
    for instance in reservation["Instances"]:
        # The next line will print the entire dictionary object
        #print(instance)
        # The next line will print only the InstanceId and Public IP Address
        if "PublicIpAddress" in instance :
            print(instance["InstanceId"] + "\t" + instance["PublicIpAddress"])
            delInstanceArray.append(str(instance["InstanceId"]))

#Delete the all the instances from the delInstanceArray
if  delInstanceArray.__len__() != 0 :
    print("deleteIstance Array: " + str(delInstanceArray) + str(delInstanceArray.__len__()))
    ec2Resource = boto3.resource('ec2',aws_access_key_id =appconfig.get('AWSConfig','aws.accessKey')
                                 , aws_secret_access_key=appconfig.get('AWSConfig','aws.accessKeySecret')
                                 , region_name=appconfig.get('AWSConfig', 'aws.region'));
    ec2Resource.instances.filter(InstanceIds = delInstanceArray).stop()
    ec2Resource.instances.filter(InstanceIds = delInstanceArray).terminate()
    print('Instances : '+ str(delInstanceArray) +' deleted successfully.')