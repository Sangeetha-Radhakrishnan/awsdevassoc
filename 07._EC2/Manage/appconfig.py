import configparser



def get_config():
    config = configparser.RawConfigParser()
    config.read('config.properties')
    return config

def get(section,key):
    return  get_config().get(section,key)